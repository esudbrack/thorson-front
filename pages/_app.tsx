import '../styles/globals.css'
import {AuthProvider, ProtectRoute} from '../contexts/auth'

function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      <ProtectRoute>
        <Component {...pageProps} />
      </ProtectRoute>
    </AuthProvider>
  )
}

export default MyApp
