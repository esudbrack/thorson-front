import React, { createContext, useState, useContext } from 'react';
import { useRouter } from 'next/router';
import { Layout } from '../components/Layout';
import api from '../services/api';


export const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);
    const router = useRouter();

    // useEffect(() => {
    //     async function loadUser() {
    //         const token = localStorage.getItem('token');
    //         if (token) {
    //             api.defaults.headers.Authorization = `Bearer ${token}`
    //             const { data: user } = await api.get('users/me')
    //             if (user) setUser(user);
    //         }
    //         setLoading(false)
    //     }
    //     loadUser()
    // }, [])

    // Faz chamada a api e seta localStorage caso sucesso
    const login = async (user) => {
        console.log(user);
        if(user.username === 'user' && user.password === '123456') {
            localStorage.setItem('token', 'token');
            setUser(user);
            router.push("/app/visualizar");
        }

        // const { data: token } = await api.post('auth/login', { username, password })
        // if (token) {
        //     localStorage.setItem('token', token);
        //     api.defaults.headers.Authorization = `Bearer ${token.token}`
        //     const { data: user } = await api.get('users/me')
        //     setUser(user)
        // }
    }

    // Remove token do localStorage e redireciona para pagina de login
    const logout = () => {
        localStorage.removeItem('token')
        console.log(router.pathname)
        setUser(null)
        // delete api.defaults.headers.Authorization
        router.push("/app/login");
    }


    return (
        <AuthContext.Provider value={{ isAuthenticated: !!user, user, login, loading, logout }}>
            {router.pathname !== '/app/login' ? 
                <Layout>
                    {children}
                </Layout>
                : 
                <>
                    {children}
                </>
            }
        </AuthContext.Provider>
    )
}

export const useAuth = () => useContext(AuthContext)

export const ProtectRoute = ({ children }) => {
    if(typeof window === "undefined") return null;

    const router = useRouter();

    const unprotectedRoutes = ['/app/login'];
    const requireAccess = unprotectedRoutes.indexOf(window.location.pathname) === -1;

    const { isAuthenticated } = useAuth();
    if (!isAuthenticated && requireAccess){
        router.push("/app/login");
        return null;
    } else {
        return children;
    } 
};